package com.experianhealth;

import com.experianhealth.controllers.GreetingController;
import com.experianhealth.controllers.WeatherController;
import com.experianhealth.diexamples.DiConstructorService;
import com.experianhealth.diexamples.DiSetterService;
import com.experianhealth.diexamples.HelloBeanImpl;
import com.experianhealth.services.EnglishGreetingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
		GreetingController controller = (GreetingController) ctx.getBean("greetingController");

        System.out.println(controller.getGreeting());

        WeatherController weatherController = ctx.getBean(WeatherController.class);

        System.out.println(weatherController.getConditions());

    }
}
