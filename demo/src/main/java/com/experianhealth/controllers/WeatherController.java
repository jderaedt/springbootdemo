package com.experianhealth.controllers;

import com.experianhealth.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created by jt on 4/26/16.
 */
@Controller
public class WeatherController {

    private WeatherService weatherService;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    public String getConditions(){

        return weatherService.getCurrentConditions();
    }

    /**
     * 1. Make this a controller
     * 2. Add a weather service
     * 3. Show current conditions in English via english impl of service - hard coding string is okay
     * 4. Show current conditions in Spanish via spanish impl of service - hard coding string is okay
     * 5. Autowire weather service into weather controller by interface type. Use Active profiles to control
     *    which version of the type is wired into controller.
     * 6. In Weather Service Impls - wire in respective hello greeting (existing greeting service impls)
     *    and output greeting when method of weather services is invoked.
     *    Autowire by name to get proper bean.
     *    6.a - use two different ways to autowire by name
     */


}
