package com.experianhealth.controllers;

import com.experianhealth.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * Created by jt on 4/26/16.
 */
@Controller
public class GreetingController {

    public GreetingController() {
        System.out.println("Building Greeting Controller");
    }

    private GreetingService greetingService;

    @Autowired

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }



    public String getGreeting(){
        return greetingService.getMyGreeting();
    }
}
