package com.experianhealth.diexamples;

import org.springframework.stereotype.Component;

/**
 * Created by jt on 4/26/16.
 */
@Component
public class HelloBeanImpl implements HelloBean {

    public HelloBeanImpl() {
        System.out.println("Spring just made a Hello bean!!!!!!!!!!!!");
    }

    public void sayHello(){
        System.out.println("Hello World!!");
    }
}
