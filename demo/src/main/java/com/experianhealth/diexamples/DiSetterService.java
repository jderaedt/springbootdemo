package com.experianhealth.diexamples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/26/16.
 */
@Service
public class DiSetterService {

    public DiSetterService() {
        System.out.println("Di Setter based service was created by Spring..........");
    }

    private HelloBean helloBean;

    @Autowired
    public void setHelloBean(HelloBean helloBean) {
        this.helloBean = helloBean;
    }

    public void showMessage(){
        helloBean.sayHello();
    }
}
