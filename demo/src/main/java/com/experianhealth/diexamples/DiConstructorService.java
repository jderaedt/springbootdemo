package com.experianhealth.diexamples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/26/16.
 */
@Service
public class DiConstructorService {

    private HelloBean helloBean;

    @Autowired
    public DiConstructorService(HelloBean helloBean) {
        this.helloBean = helloBean;
        System.out.println("Di Constructor Service was wired by Spring");
    }

    public void showMessage(){
        helloBean.sayHello();
    }
}
