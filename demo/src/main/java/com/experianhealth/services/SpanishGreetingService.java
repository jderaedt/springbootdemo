package com.experianhealth.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/26/16.
 */
@Service
@Profile({"spanish", "default"})
public class SpanishGreetingService implements GreetingService{

    public SpanishGreetingService() {
        System.out.println("Hola, creating a spanish greeting service");
    }

    @Override
    public String getMyGreeting() {
        return "Hola Mundo!!";
    }
}
