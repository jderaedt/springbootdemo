package com.experianhealth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/27/16.
 */
@Service
@Profile("english")
public class EnglishWeatherServiceImpl implements WeatherService {

    private GreetingService greetingService;

    @Autowired
    @Qualifier("englishGreetingService")
    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    @Override
    public String getCurrentConditions() {

        System.out.println(greetingService.getMyGreeting());

        return "Its nice in English";
    }
}
