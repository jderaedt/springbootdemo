package com.experianhealth.services;

/**
 * Created by jt on 4/27/16.
 */
public interface WeatherService {
    String getCurrentConditions();
}
