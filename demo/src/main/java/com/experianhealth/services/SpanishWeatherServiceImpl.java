package com.experianhealth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/27/16.
 */
@Service
@Profile("spanish")
public class SpanishWeatherServiceImpl implements WeatherService {

    private GreetingService spanishGreetingService;

    @Autowired
    public void setSpanishGreetingService(GreetingService spanishGreetingService) {
        this.spanishGreetingService = spanishGreetingService;
    }

    @Override
    public String getCurrentConditions() {
        System.out.println(spanishGreetingService.getMyGreeting());

        return "Its nice in Spanish";
    }
}
