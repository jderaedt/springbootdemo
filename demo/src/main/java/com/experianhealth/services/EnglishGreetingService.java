package com.experianhealth.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by jt on 4/26/16.
 */
@Service
@Profile({"english", "default"})
@Primary
public class EnglishGreetingService implements GreetingService {

    public EnglishGreetingService() {
        System.out.println("Building English Greeting Service");
    }

    @Override
    public String getMyGreeting() {
        return "Hello World - English";
    }
}
